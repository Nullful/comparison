﻿using System;
using System.Text;

namespace comparison
{
    public static class Levenshtein
    {
        public static Prescription LevenshteinDistance(string string1, string string2) 
        {
            int firstStringLength = string1.Length, secondStringLength = string2.Length;
            int[,] levenshteinMatrix = new int[firstStringLength + 1, secondStringLength + 1]; // Матрица для вычисления дистанции 
            char[,] actionsMatrix = new char[firstStringLength + 1, secondStringLength + 1]; // Матрица для хранения действий

            // Базовые значения
            for (int i = 0; i <= firstStringLength; i++) 
            {
                levenshteinMatrix[i, 0] = i;
                actionsMatrix[i, 0] = 'D';
            }
            for (int i = 0; i <= secondStringLength; i++) 
            {
                levenshteinMatrix[0, i] = i;
                actionsMatrix[0, i] = 'I';
            }

            for (int i = 1; i <= firstStringLength; i++)
            {
                for (int j = 1; j <= secondStringLength; j++)
                {
                    int cost = (string1[i - 1] != string2[j - 1]) ? 1 : 0;
                    if (levenshteinMatrix[i, j - 1] < levenshteinMatrix[i - 1, j] && levenshteinMatrix[i, j - 1] < levenshteinMatrix[i - 1, j - 1] + cost)
                    {
                        // Вставка
                        levenshteinMatrix[i, j] = levenshteinMatrix[i, j - 1] + 1;
                        actionsMatrix[i, j] = 'I';
                    }
                    else if (levenshteinMatrix[i - 1, j] < levenshteinMatrix[i - 1, j - 1] + cost)
                    {
                        // Удаление
                        levenshteinMatrix[i, j] = levenshteinMatrix[i - 1, j] + 1;
                        actionsMatrix[i, j] = 'D';
                    }
                    else
                    {
                        // Замена или совпадение
                        levenshteinMatrix[i, j] = levenshteinMatrix[i - 1, j - 1] + cost;
                        actionsMatrix[i, j] = (cost == 1) ? 'R' : 'M';
                    }
                }
            }

            // Восстановление предписания
            StringBuilder route = new StringBuilder("");
            int ii = firstStringLength, jj = secondStringLength;
            do {
                char actionCharacter = actionsMatrix[ii, jj];
                route.Append(actionCharacter);
                if (actionCharacter == 'R' || actionCharacter == 'M') {
                    ii--;
                    jj--;
                }
                else if (actionCharacter == 'D') {
                    ii--;
                }
                else {
                    jj--;
                }
            } while (ii != 0 || jj != 0);

            char[] reversedRoute = route.ToString().ToCharArray(); // Реверсим строку чтобы получить путь слева направо, а не наоборот
            Array.Reverse(reversedRoute);
            
            return new Prescription(levenshteinMatrix[firstStringLength, secondStringLength], new String(reversedRoute));
        }
        public static string CompareByRoute(string source, string target)
        {
            Prescription prescription = LevenshteinDistance(source, target);
            string deletedString = "";
            string addedString = "";
            string resultString = "";
            
            // Проходим весь путь, полученый вследствии вычсления расстояния Левенштейна, удаленные символы добавляем в [], добавленные в ()
            for (int routeCounter = 0, string1 = 0, string2 = 0; routeCounter < prescription.Route.Length; routeCounter++) { 
                switch (prescription.Route[routeCounter])
                {
                    case 'M':
                    {
                        if (deletedString.Length > 0)
                        {
                            resultString += "[" + deletedString + "]";
                            deletedString = "";
                        }
                        if (addedString.Length > 0)
                        {
                            resultString += "(" + addedString + ")";
                            addedString = "";
                        }

                        resultString += source[string1];
                        string1++;
                        string2++;
                        break;
                    }
                    case 'D':
                    {
                        deletedString += source[string1];
                        string1++;
                        break;
                    }
                    case 'I':
                    {
                        addedString += target[string2];
                        string2++;
                        break;
                    }
                    case 'R':
                    {
                        deletedString += source[string1];
                        addedString += target[string2];
                        string1++;
                        string2++;
                        break;
                    }
                }
            }

            // Если в конце сравнения остались символы, то вставляем их
            if (deletedString.Length > 0)
                resultString += "[" + deletedString + "]";
            if (addedString.Length > 0)
                resultString += "(" + addedString + ")";

            return resultString;
        }
    }
}