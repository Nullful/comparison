﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace comparison
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length == 2)
            {
                try
                {
                    StreamReader source_file = new StreamReader(args[0]);
                    StreamReader target_file = new StreamReader(args[1]);

                    string result_file_name = "result.txt";
                    StreamWriter result_file = new StreamWriter(new FileStream(result_file_name, FileMode.Create));

                    string source = source_file.ReadToEnd();
                    string target = target_file.ReadToEnd();

                    if (source.Length < 1 && target.Length < 1)
                        throw new Exception("Both files are empty");
                    
                    string result = Levenshtein.CompareByRoute(source, target);

                    result_file.Write(result);

                    source_file.Close();
                    target_file.Close();
                    result_file.Close();

                    Console.WriteLine("Success.");
                }
                catch (FileNotFoundException e)
                {
                    Console.WriteLine("File " + e.FileName + " has not been found");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            else
            {
                Console.WriteLine(@"Invalid syntax. Valid is: ""comparison path1 path2""");
            }
        }
    }
}