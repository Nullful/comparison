﻿using System.IO;
using NUnit.Framework;

namespace comparison
{
    public class Tests
    {
        private static string source = "Deleted. To Sherlock Holmes she is always the woman. I have seldom heard him mention her under any other name.";
        private static string target = "To Sherlock Sholmes she is always the man. I have seldom beard him mention her under any other name. Added.";
        private static string expected =
            "[Deleted. ]To Sherlock [H](Sh)olmes she is always the [wo]man. I have seldom [h](b)eard him mention her under any other name.( Added.)";

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ResultStringTest()
        {
            Assert.AreEqual(Levenshtein.CompareByRoute(source, target), expected);
        }
    }
}