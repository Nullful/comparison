﻿namespace comparison
{
    public class Prescription // Класс Prescription хранит в себе массив последовательных действий (Route) и  мин. кол-во действий для превращения одной строки в другую (Distance) 
    {
        public string Route { get; }

        public int Distance { get; }
        public Prescription(int distance, string route) {
            Distance = distance;
            Route = route;
        }
    }
}