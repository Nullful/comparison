I.

[Deleted. ]To Sherlock [H](Sh)olmes she is always the [wo]man. I have seldom [h](b)eard him mention her under any other name.( Added.)

I had seen little of Holmes lately. My marriage had drifted us away from each other. 

"I see it, I deduce it. How do I know that you have been getting yourself very wet lately, a(dded text for test a)nd that you have a most clumsy and careless servant girl?"

"My dear Holmes," said I, "this is too much. You would certainly have been burned, had you lived a few centuries ago. 
It is true that I had a country walk on Thursday and came home in a dreadful mess, but as I have changed my clothes I can't imagine how you deduce it.[ ]

He chuckled to himself and rubbed his long, nervous [h](w)ands together.

["]I[t] [is simpli]c[ity itself," said he; "my eyes tell me that ]o[n the inside of yo]u[r ]l[eft shoe, just where the firelight strikes it, the leather is score]d [by six almost parallel cuts. 

I could ]not help laughing at the ease with which he explained his process of deduction. 
"[W](t)he( thi)n(g) [I](always) [h](app)ear(s) [y](t)o[u]( me) [giv](to b)e [y](s)o[u]( )r[ reas](idicul)o[n](u)s[,"](ly) [I ](you)r[ema]( p)r[k](oc)e[d,](ss.) ["]And yet I believe that my eyes are as good as yours."

"Quite so," he answered, lighting a cigarette, and throwing himself down into an armchair. 
"You see, but you do not observe. The distinction is clear. For example, you have frequently seen the steps which lead up from the hall to this room."

"Freq(qwe)uently."

"How often?"

"Well, some hundreds of times."

"Then how many are there?"

"How many? I don't know."

"Quite so! You have not observed. And yet you have seen. That is just my point.[ ]
[
The note was undated, and without either signature or address.]